package Ex2;

import javax.swing.*;

public class Ex2 {

    private static JFrame jFrame = new JFrame();
    private static JTextField first = new JTextField();
    private static JTextField second = new JTextField();
    private static JTextField sum = new JTextField();
    private static JButton button = new JButton();

    public static void main(String[] args) {
        jFrame.setTitle("Sum chars");
        jFrame.setLayout(null);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setSize(300,250);

        first.setBounds(20,20,100,30);
        jFrame.getContentPane().add(first);

        second.setBounds(20,60,100,30);
        jFrame.getContentPane().add(second);

        sum.setBounds(20,100,100,30);
        sum.setEditable(false);
        jFrame.getContentPane().add(sum);

        button.setBounds(20,150,150,30);
        button.setText("Sum");
        button.addActionListener(e -> {
            sum.setText("" + (first.getText().length() + second.getText().length()));
        });

        jFrame.getContentPane().add(button);
        jFrame.setVisible(true);
    }
}
